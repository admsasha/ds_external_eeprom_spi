#include "ds_external_eeprom_spi.h"

// for AT25LC04, pin 10
ds_external_eeprom_spi eeprom(ds_external_eeprom_spi::MODEL_25_04,10);

void setup(){
    Serial.begin(9600);

    Serial.println("Write byte to EEPROM memory...");
    eeprom.writeByte(0,0xAA);

    Serial.print("Read byte from EEPROM memory... ");
    Serial.println(eeprom.readByte(0), HEX);

    Serial.println("\n\n");

    // Declare byte arrays.
    byte inputBytes[32] = { 0 };
    byte outputBytes[32] = { 0 };

    // Fill input array. 
    for (byte i = 0; i < 32; i++) inputBytes[i] = i;
    
    // Write input array to EEPROM memory.
    Serial.println("Write bytes to EEPROM memory...");
    eeprom.writeBytes(0x00, inputBytes,32);
	
    // Read array with bytes read from EEPROM memory.
    Serial.println("Read bytes from EEPROM memory...");
    eeprom.readBytes(0x00, outputBytes,32);

    for (byte i = 0; i < 32; i++){
        Serial.print(outputBytes[i], HEX);	Serial.print(" ");
    }
}

void loop(){

}


