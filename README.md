# ds_external_eeprom_spi

EEPROM 25LC04 - 25LC512 library for Arduino

Универсальная библиотека для работы с внешней EEPROM по SPI протоколу (25XX02 - 25XX512)

Чтение/запись одного байта или массива байт.

Работа с микросхемами такие как 25xx04, 25xx16, 25xx32, 25xx64, 25xx256 и т.д.

# Functions
```C++
bool check();

// read/write byte
byte readByte(int address);
void writeByte(int address, byte data);

// read/write bytes
void readBytes(int address, byte* p_buffer, int length);
void writeBytes(int address, byte* p_data, int length);
```

# Examples
* [write_read_eeprom.ino](https://bitbucket.org/admsasha/ds_external_eeprom_spi/src/master/examples/write_read_eeprom/write_read_eeprom.ino)
