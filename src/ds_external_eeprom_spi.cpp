#include "ds_external_eeprom_spi.h"

#include <SPI.h>

const byte READ  = 0b00000011;	// Read data from memory array beginning at selected address
const byte WRITE = 0b00000010;  // Write data to memory array beginning at selected address
const byte WRDI  = 0b00000100;	// Reset the write enable latch (disable write operations)
const byte WREN  = 0b00000110;	// Set the write enable latch (enable write operations)
const byte RDSR  = 0b00000101;	// Read STATUS register
const byte WRSR  = 0b00000001;	// Write STATUS register

ds_external_eeprom_spi::ds_external_eeprom_spi(EEPROM_MODEL model, byte pin_ss){
	if (model==EEPROM_MODEL::MODEL_25_02) init(2,16,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_04) init(4,16,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_08) init(8,16,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_16) init(16,16,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_32) init(32,32,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_64) init(64,32,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_128) init(128,64,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_256) init(256,64,pin_ss);
	if (model==EEPROM_MODEL::MODEL_25_512) init(512,128,pin_ss);
}

ds_external_eeprom_spi::ds_external_eeprom_spi(unsigned int eeprom_size_kbit, unsigned int pageSize, byte pin_ss){
	init(eeprom_size_kbit,pageSize,pin_ss);
}

void ds_external_eeprom_spi::init(unsigned int eeprom_size_kbit, unsigned int pageSize, byte pin_ss){
	SPI.begin();

	_eeprom_size_kbit = eeprom_size_kbit;
	_pageSize = pageSize;
	_pin_ss = pin_ss;

	pinMode(_pin_ss,OUTPUT);
	digitalWrite(_pin_ss,HIGH);
}


// enable write operations
void ds_external_eeprom_spi::eeprom_enable_write(){
	digitalWrite(_pin_ss,LOW);
	SPI.transfer(WREN);
	digitalWrite(_pin_ss,HIGH);
}

// disable write operations
void ds_external_eeprom_spi::eeprom_disable_write(){
	digitalWrite(_pin_ss,LOW);
	SPI.transfer(WRDI);
	digitalWrite(_pin_ss,HIGH);
}

void ds_external_eeprom_spi::writeByte(int address, byte value){
	eeprom_enable_write();

	digitalWrite(_pin_ss,LOW);
	if (_eeprom_size_kbit==4 and address>=256){
		SPI.transfer(WRITE | (1<<3));
	}else{
		SPI.transfer(WRITE);
	}

	sendAddress(address);
	SPI.transfer(value);
	digitalWrite(_pin_ss,HIGH);

	eeprom_disable_write();
	delay(10);
}



void ds_external_eeprom_spi::writeBytes(int address,byte* p_data, int length){
	int nPage = floor(address/_pageSize);		// ��������� ����� ��������

	int remainder_write = length;
	int page_size = _pageSize;
	int pos_buffer = 0;
	uint8_t countWrite = 0;

	countWrite=(_pageSize-(address-nPage*_pageSize));

	while(remainder_write>0){	
		writePage(address+pos_buffer,p_data,pos_buffer,countWrite);
		
		pos_buffer += countWrite;
		remainder_write -= countWrite;

		if (remainder_write>page_size){
			countWrite = page_size;
		}else{
			countWrite = remainder_write;
		}
	}
}

void ds_external_eeprom_spi::writePage(int address, byte* p_buffer, int start, int length){
	eeprom_enable_write();

	digitalWrite(_pin_ss,LOW);
	if (_eeprom_size_kbit==4 and address>=256){
		SPI.transfer(WRITE | (1<<3));
	}else{
		SPI.transfer(WRITE);
	}
	sendAddress(address);

	for (int i=0;i<length;i++){
		SPI.transfer(p_buffer[start+i]);
	}
	digitalWrite(_pin_ss,HIGH);
	eeprom_disable_write();

	delay(10);
}


byte ds_external_eeprom_spi::readByte(int address){
	byte result=0xff;

	digitalWrite(_pin_ss,LOW);
	if (_eeprom_size_kbit==4 and address>=256){
		SPI.transfer(READ | (1<<3));
	}else{
		SPI.transfer(READ);
	}

	sendAddress(address);
	result = SPI.transfer(0xFF);
	digitalWrite(_pin_ss,HIGH);

	return result;
}

void ds_external_eeprom_spi::readBytes(int address, byte* p_buffer, int length){
	digitalWrite(_pin_ss,LOW);
	if (_eeprom_size_kbit==4 and address>=256){
		SPI.transfer(READ | (1<<3));
	}else{
		SPI.transfer(READ);
	}
	sendAddress(address);
	for(int i=0;i<length;i++){
		p_buffer[i] = SPI.transfer(0xFF);
	}
	digitalWrite(_pin_ss,HIGH);
}


void ds_external_eeprom_spi::sendAddress(int address){
	if (_eeprom_size_kbit<=4){
		SPI.transfer(address);
	} else if (_eeprom_size_kbit<1024){
		SPI.transfer((address >> 8));
		SPI.transfer(address);
	}else{
		SPI.transfer((address >> 16));
		SPI.transfer((address >> 8));
		SPI.transfer(address);
	}
}
