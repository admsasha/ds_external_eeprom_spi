#ifndef DS_EXTERNAL_EEPROM_SPI_H
#define DS_EXTERNAL_EEPROM_SPI_H
/*
	���������� ��� ������ � ������� EEPROM �� SPI ��������� (25XX02 - 25XX512)
	������ � ������������ ����� ��� AT25LC04, AT25LC16, AT25LC32 � �.�.

	email: dik@Inbox.ru
	www: http://dansoft.ru
*/

#include <Arduino.h>

class ds_external_eeprom_spi {
	public:
		enum EEPROM_MODEL {
			MODEL_25_02, MODEL_25_04, MODEL_25_08, MODEL_25_16, MODEL_25_32, MODEL_25_64, MODEL_25_128, MODEL_25_256, MODEL_25_512
		};

		ds_external_eeprom_spi(EEPROM_MODEL model, byte pin_ss);
		ds_external_eeprom_spi(unsigned int eeprom_size_kbit, unsigned int pageSize, byte pin_ss);

		void writeByte(int address, byte value);
		void writeBytes(int address,byte* p_data, int length);
		
		byte readByte(int address);
		void readBytes(int address, byte* p_buffer, int length);

	private:
		void init(unsigned int eeprom_size_kbit, unsigned int pageSize, byte pin_ss);

		void eeprom_enable_write();
		void eeprom_disable_write();

		void writePage(int address, byte* p_buffer, int start, int length);

		void sendAddress(int address);


		byte _pin_ss;
		unsigned int _eeprom_size_kbit;
		unsigned int _pageSize;

};


#endif
